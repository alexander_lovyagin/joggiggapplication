package com.exam.alovyagin.jogging;


import com.exam.alovyagin.jogging.model.Jogging;
import com.exam.alovyagin.jogging.model.User;
import com.exam.alovyagin.jogging.repository.JoggingRepository;
import com.exam.alovyagin.jogging.repository.UserRepository;
import com.exam.alovyagin.jogging.service.JoggingService;
import com.exam.alovyagin.jogging.util.LogFactory;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        DataConfig.class,
        SecurityConfig.class,
        MVCConfig.class,
        JoggingApplication.class})
@Transactional
@WebAppConfiguration
public class CommonTest {
    private final Logger log = LogFactory.getLogger(this.getClass());



	@Before
	public void beforeAll(){		
        log.info("\n\nstart testing\n\n");
	}


    @Autowired
    private UserRepository userRepository;

    @Test
    public void userRepositoryTest(){
        List<User> list = userRepository.findAll();

        log.info("\n\n\nuserRepositoryTest");
        list.stream().forEach(System.out::println);
        assertNotNull(list);
    }



    @Autowired
    private JoggingRepository joggingRepository;

    @Test
    public void joggingRepositoryTest(){
        log.info("\n\n\njoggingRepositoryTest");
        List<Jogging>joggingList = org.assertj.core.util.Lists.newArrayList(joggingRepository.findAll());
        joggingList.stream().forEach(System.out::println);

        assertNotNull(joggingList);


    }


    @Autowired
    private JoggingService joggingService;

    @Test
    public void joggingServiceTest(){
        List<Jogging>  joggingList = joggingService.getFullJoggingListForPtintingTest();
        log.info("\n\n\njoggingServiceTest");
        joggingList.stream().forEach(System.out::println);

        assertNotNull(joggingList);
    }

   
    @After
    public void afterAll(){
        log.info("call ending code\n test complete\n\n\n");
    }
}