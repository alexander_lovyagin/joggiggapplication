package com.exam.alovyagin.jogging.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * this class contains some Json annotations, because it can be advisable for REST applications
 * but best way is splitting into 2 classes.
 *
 */

@Entity
@Table(name = "jogging")
@JsonTypeName("Jogging")
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class Jogging implements Comparable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone = "Europe/Moscow")
    @Column(nullable = false)
    private Timestamp joggingDate;

    private Integer timeBySeconds;

    private Integer distanceByMeters;

    private Float speedByKmpH;

    @Column(length = 512)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    @JsonBackReference(value = "user-joggings")
    private User user;


    // TODO: 25.04.2017 use lombok

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (id == null || o == null || getClass() != o.getClass())
            return false;
        Jogging j = (Jogging) o;
        return id.equals(j.id);
    }

    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }

    @Override
    public String toString() {
        return "Jogging{" +
                "id=" + id +
                ", joggingDate=" + joggingDate  +
                ", timeBySeconds=" + timeBySeconds +
                ", distanceByMeters=" + distanceByMeters +
                ", speedByKmpH=" + speedByKmpH +
                ", description='" + description + '\'' +
                ", user='" + (user == null ? "nobody" : user.getId()+ ":" + user.getName()) +"'"+
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Jogging j = (Jogging) o;
        return id.compareTo(j.id);
    }


    //region getters & setters

    public Long getId() {
        return id;
    }

    public Jogging setId(Long id) {
        this.id = id;
        return this;
    }

    public Timestamp getJoggingDate() {
        return joggingDate;
    }

    public Jogging setJoggingDate(Timestamp joggingDate) {
        this.joggingDate = joggingDate;
        return this;
    }

    public Integer getTimeBySeconds() {
        return timeBySeconds;
    }

    public Jogging setTimeBySeconds(Integer timeBySeconds) {
        this.timeBySeconds = timeBySeconds;
        return this;
    }

    public Integer getDistanceByMeters() {
        return distanceByMeters;
    }

    public Jogging setDistanceByMeters(Integer distanceByMeters) {
        this.distanceByMeters = distanceByMeters;
        return this;
    }

    public Float getSpeedByKmpH() {
        return speedByKmpH;
    }

    public Jogging setSpeedByKmpH(Float speedByKmpH) {
        this.speedByKmpH = speedByKmpH;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Jogging setDescription(String description) {
        this.description = description;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Jogging setUser(User user) {
        this.user = user;
        return this;
    }


    //endregion
}
