package com.exam.alovyagin.jogging.controller;

import com.exam.alovyagin.jogging.model.Jogging;
import com.exam.alovyagin.jogging.model.User;
import com.exam.alovyagin.jogging.repository.JoggingRepository;
import com.exam.alovyagin.jogging.repository.UserRepository;
import com.exam.alovyagin.jogging.service.JoggingService;
import com.exam.alovyagin.jogging.util.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.sql.Timestamp;

/**
 * Jogging management controller.
 *
 */
@Controller
@EnableJpaRepositories("com.exam.alovyagin.jogging.repository")
public class JoggingController {
    Logger log = LogFactory.getLogger(JoggingController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JoggingRepository joggingRepository;

    @Autowired
    private JoggingService joggingService;


    /**
     * Getting user jogging list method.
     *
     * @param model
     * @return
     */
    @RequestMapping(value ={"/","/list"}, method = RequestMethod.GET)
    public String getList(Model model, Principal principal){
        /**
         * Only for convenience and simplification front
         */
        model.addAttribute("jogging",
                new Jogging().setJoggingDate(new Timestamp(System.currentTimeMillis())));

        model.addAttribute("joggingList", joggingService.findByPrincipal(principal));

        return "jogging";
    }


    /**
     * Add user jogging method.
     *
     * @param jogging
     * @param principal
     * @return
     */
    @RequestMapping(value = "/set", method = RequestMethod.POST)
    public String setJogging(
            @ModelAttribute("jogging") Jogging jogging, Principal principal){


        String username = principal.getName();
        User user = userRepository.findByName(username);

        Long id = jogging.getId();

        if (id != null && id >0) {

            /**
             * primitive security, demo only
             */

            Jogging j = joggingRepository.findOne(id);

            if (!username.equals(j.getUser().getName())) {
                log.error("Security case detected on edit jogging: user "+username);
                return "redirect:/list";
            }
        }

        jogging.setUser(user);
        joggingService.saveOrUpdate(jogging);

        return "redirect:/list";
    }


    /**
     * Fill jogging on form for editing.
     *
     * @param id
     * @param model
     * @param principal
     * @return
     */
    @RequestMapping(value ={"/edit/{id}"}, method = RequestMethod.GET)
    public String edit(@PathVariable("id") Long id, Model model, Principal principal){


        if (id != null && id >0) {
            model.addAttribute("jogging", joggingRepository.findOne(id));
        }else {
            model.addAttribute("jogging",
                new Jogging().setJoggingDate(new Timestamp(System.currentTimeMillis())));
        }

        model.addAttribute("joggingList", joggingService.findByPrincipal(principal));

        return "jogging";
    }

    /**
     * Remove user jogging method.
     *
     * @param id
     * @return
     */
    @RequestMapping("/remove/{id}")
    public String removeJogging(@PathVariable("id") Long id, Principal principal){

        /**
         * primitive security, demo only
         */

        String username = principal.getName();
        Jogging jogging = joggingRepository.findOne(id);

        if (username.equals(jogging.getUser().getName())) {
            joggingRepository.delete(id);
        }else{
            log.error("Security case detected on remove jogging: user "+username);
        }

        return "redirect:/list";
    }




    /**
     * Test method like ping
     * @return
     */
    @RequestMapping(value = {"/test"}, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    String testOk() {
        return "{\"status\":\"ok\"}";
    }
}
