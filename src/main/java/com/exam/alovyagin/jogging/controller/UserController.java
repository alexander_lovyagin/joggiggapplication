package com.exam.alovyagin.jogging.controller;

import com.exam.alovyagin.jogging.model.User;
import com.exam.alovyagin.jogging.repository.UserRepository;
import com.exam.alovyagin.jogging.service.SecurityService;
import com.exam.alovyagin.jogging.service.UserService;
import com.exam.alovyagin.jogging.util.LogFactory;
import com.exam.alovyagin.jogging.validator.UserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User management controller.
 *
 */
@Controller
@RequestMapping(value = "users")
@EnableJpaRepositories("com.exam.alovyagin.jogging.repository")
public class UserController {
    Logger log = LogFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository  userRepository;

    @Autowired
    private UserService     userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator   userValidator;

    /**
     * Authentication method
     * @param model
     * @param error
     * @param logout
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null) {
            model.addAttribute("error", "Username or password is incorrect.");
        }
        if (logout != null) {
            model.addAttribute("message", "Log out successfully.");
        }

        return "login";
    }

    /**
     * Log out method.
     *
     * @param response
     * @return
     */
    @RequestMapping(value="/logout", method = RequestMethod.POST)
    public String logoutPage (HttpServletRequest request,
                              HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/users/login";

    }


    /**
     * User registration prompt method.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        log.info("registration GET");

        model.addAttribute("userForm", new User());

        return "registration";
    }

    /**
     * User registration form method.
     *
     * @param userForm
     * @param bindingResult
     * @param model
     * @return
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(
            @ModelAttribute("userForm") User userForm,
            BindingResult bindingResult,
            Model model) {

        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);
        securityService.autoLogin(userForm.getName(), userForm.getConfirmPassword());
        return "redirect:/list";
    }



}
