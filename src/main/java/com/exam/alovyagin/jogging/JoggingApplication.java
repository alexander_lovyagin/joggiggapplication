package com.exam.alovyagin.jogging;
import com.exam.alovyagin.jogging.util.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
@EnableJpaRepositories("com.exam.alovyagin.repository")
public class JoggingApplication  extends SpringBootServletInitializer {
    private static final Logger log = LogFactory.getLogger(JoggingApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(JoggingApplication.class);
    }

    public static void main(String... args) {


        /**
         * just fine hello prompt
         */
        final String hello =
        "\n\n" +
        "     _                   _                  _                _ _           _   _             \n" +
        "    | | ___   __ _  __ _(_)_ __   __ _     / \\   _ __  _ __ | (_) ___ __ _| |_(_) ___  _ __  \n" +
        " _  | |/ _ \\ / _` |/ _` | | '_ \\ / _` |   / _ \\ | '_ \\| '_ \\| | |/ __/ _` | __| |/ _ \\| '_ \\\n" +
        "| |_| | (_) | (_| | (_| | | | | | (_| |  / ___ \\| |_) | |_) | | | (_| (_| | |_| | (_) | | | |\n" +
        " \\___/ \\___/ \\__, |\\__, |_|_| |_|\\__, | /_/   \\_\\ .__/| .__/|_|_|\\___\\__,_|\\__|_|\\___/|_| |_|\n" +
        "             |___/ |___/         |___/          |_|   |_|                                    " +
        "\n\n" +
        "             :: version  0.1 ::\n\n";

        log.info(hello);
        SpringApplication.run(JoggingApplication.class, args);
    }
}
