package com.exam.alovyagin.jogging.repository;

import com.exam.alovyagin.jogging.model.Jogging;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Repository for {@link Jogging}
 *
 */

public interface JoggingRepository extends PagingAndSortingRepository<Jogging, Long> {
    List<Jogging> findByUserId(Long user_id);
}
