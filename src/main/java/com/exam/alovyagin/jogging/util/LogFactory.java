package com.exam.alovyagin.jogging.util;

import org.apache.log4j.*;

import java.io.PrintWriter;

/**
 * Custom logging management by {@link Logger}
 */
public class LogFactory {
    private final static ConsoleAppender CONSOLE_APPENDER;
    private final static DailyRollingFileAppender dailyRollingFileAppender;
    private final static RollingFileAppender rollingFileAppender;
    private final static Logger rootLogger;


    static {
        PatternLayout layout = new PatternLayout();
        layout.setConversionPattern("%d{dd-MM-yyyy HH:mm:ss,SSS} %-5p %c{2}  - %m%n"); //%M (%L)

        String fileName;

        /**
         *  consoleAppender
         */
        CONSOLE_APPENDER = new ConsoleAppender();
        CONSOLE_APPENDER.setWriter(new PrintWriter(System.out));
        CONSOLE_APPENDER.setLayout(new PatternLayout(PatternLayout.TTCC_CONVERSION_PATTERN));
        CONSOLE_APPENDER.setName("stdout");


        /**
         * dailyRollingFileAppender
         */
        dailyRollingFileAppender = new DailyRollingFileAppender();
        dailyRollingFileAppender.setLayout(layout);

        dailyRollingFileAppender.setDatePattern("'_'yyyy-MM-dd'.log'");

        fileName = "log/jogging.log";

        dailyRollingFileAppender.setEncoding("UTF-8");

        dailyRollingFileAppender.setFile(fileName);
        dailyRollingFileAppender.setAppend(true);
        dailyRollingFileAppender.activateOptions();


        /**
         * rollingFileAppender
         */
        rollingFileAppender = new RollingFileAppender();
        rollingFileAppender.setLayout(layout);

        fileName = "log/root.log";

        rollingFileAppender.setEncoding("UTF-8");

        rollingFileAppender.setFile(fileName);
        rollingFileAppender.setAppend(true);
        rollingFileAppender.setMaxFileSize("5MB");
        rollingFileAppender.setMaxBackupIndex(10);
        rollingFileAppender.activateOptions();


        /**
         * rootLogger
         */
        rootLogger = Logger.getRootLogger();
        rootLogger.setLevel(Level.INFO);
        rootLogger.addAppender(rollingFileAppender);


    }

    /**
     * Get logger for class
     * @param clazz
     * @return
     */
    public static Logger getLogger(Class clazz) {
        Logger log = Logger.getLogger(clazz.getSimpleName());
        log.setLevel(Level.DEBUG);
        log.addAppender(CONSOLE_APPENDER);

        return log;
    }
}
