package com.exam.alovyagin.jogging.validator;

import com.exam.alovyagin.jogging.model.User;
import com.exam.alovyagin.jogging.repository.UserRepository;
import com.exam.alovyagin.jogging.util.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


/**
 * Custom implementation of {@link Validator}
 *
 */

@Component
public class UserValidator implements Validator {
    private final Logger log = LogFactory.getLogger(UserValidator.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }


    /**
     * Validate user by custom rules.
     *
     * @param target
     * @param errors
     */
    @Override
    public void validate(Object target, Errors errors) {
        log.info("validate");
        User user = (User) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "Required" ,"This field is required");
        if (user.getName().length() < 3 || user.getName().length() > 32) {
            errors.rejectValue("name", "Size.userForm.username","Username must be between 3 and 32 characters.");
        }

        if (userRepository.findByName(user.getName()) != null) {
            errors.rejectValue("name", "Duplicate.userForm.username", "Username is already exists");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Required" ,"This field is required");
        if (user.getPassword().length() < 3 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password", "Password must be between 3 and 32 characters.");
        }

        if (!user.getConfirmPassword().equals(user.getPassword())) {
            errors.rejectValue("confirmPassword", "Different.userForm.password", "Password don't match.");
        }
    }
}
