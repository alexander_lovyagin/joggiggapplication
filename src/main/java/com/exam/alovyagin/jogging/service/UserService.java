package com.exam.alovyagin.jogging.service;

import com.exam.alovyagin.jogging.model.User;

/**
 * Interface for User Service.
 *
 */
public interface UserService {
     void save(User user);
     User findByName(String name);
}
