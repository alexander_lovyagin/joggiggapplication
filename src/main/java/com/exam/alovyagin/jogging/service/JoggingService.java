package com.exam.alovyagin.jogging.service;

import com.exam.alovyagin.jogging.model.Jogging;
import com.exam.alovyagin.jogging.model.User;

import java.security.Principal;
import java.util.List;

/**
 * Interface for Jogging Service.
 *
 */
public interface JoggingService {
    List<Jogging> findByPrincipal(Principal principal);

    void saveOrUpdate(Jogging jogging);
    void removeByIdAndUser(Long id, User user);

    List<Jogging>  getFullJoggingListForPtintingTest();
}
