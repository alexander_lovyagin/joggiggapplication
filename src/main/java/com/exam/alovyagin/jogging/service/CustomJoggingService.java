package com.exam.alovyagin.jogging.service;

import com.exam.alovyagin.jogging.model.Jogging;
import com.exam.alovyagin.jogging.model.User;
import com.exam.alovyagin.jogging.repository.JoggingRepository;
import com.exam.alovyagin.jogging.repository.UserRepository;
import com.exam.alovyagin.jogging.util.LogFactory;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

/**
 * Application Service.
 *
 */
@Service
@Repository
public class CustomJoggingService implements JoggingService {
    private final static Logger log = LogFactory.getLogger(CustomJoggingService.class);

    @Autowired
    private JoggingRepository joggingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionFactory sessionFactory;


    /**
     * Find Jogging list by Principal and DESC sort
     * @param principal
     * @return
     */

    @Override
    public List<Jogging> findByPrincipal(Principal principal) {

        if (principal == null) return Collections.emptyList();

        String username = principal.getName();
        User user = userRepository.findByName(username);

        if (user == null) return Collections.emptyList();

        return
            joggingRepository.findByUserId(
                    user.getId()
    //                ,new Sort(Sort.Direction.ASC, "joggingDate")
            );
    }


    @Override
    public void saveOrUpdate(Jogging jogging) {

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.saveOrUpdate(jogging);
            log.info("Jogging successfully saveOrUpdate.: " + jogging);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            log.error(e);
        }
        finally {
            session.close();
        }
    }

    @Override
    public void removeByIdAndUser(Long id, User user) {

        joggingRepository.delete(id);

//        Session session = this.sessionFactory.openSession();
//        Jogging jogging = session.load(Jogging.class, id);
//        Transaction tx = session.beginTransaction();
//        try {
//            if(jogging!=null && user.equals(jogging.getUser())) {
//                session.delete(jogging);
//            }
//            log.info("Jogging successfully removed. Jogging details: " + jogging);
//            tx.commit();
//        }catch (Exception e){
//            tx.rollback();
//            log.error(e);
//        }
//        finally {
//            session.close();
//        }

    }


    /**
     * get {@link Jogging} and {@link User}  joined list
     * @return
     */
    public List<Jogging>  getFullJoggingListForPtintingTest(){
        Session session = sessionFactory.openSession();

        List  joggingList = session.createQuery(
                "select  jogging  " +
                "from Jogging as jogging " +
                "join jogging.user ")
                .list();


        return (List<Jogging>) joggingList;
    }
}
