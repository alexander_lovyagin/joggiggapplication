package com.exam.alovyagin.jogging.service;

/**
 * Interface for Security Service.
 *
 */
public interface SecurityService {
    String findLoggedInUsername();
    void autoLogin(String username, String password);
}
